# CHANGE

## v0.3 - nonymous voting

### major

+ added the [`Bridge` contract signature](https://tezos-southeast-asia.gitlab.io/incentives/2019/04/18/pattern001_bridge/)
+ added callback `address`

```mermaid
graph TB
   style X fill:#BBB
   style M fill:#BBB

   style B fill:#BBF
   style H fill:#BBF


   X -.-> |1. apply| C
   M -.-> |3. vote*| V
   V --> |4. trigger| B
   B --> |5. inform| H

   subgraph voting applier
      subgraph off-chain
         X[Voting<br>Initiator]
         M[Committee<br>Members]
      end
      subgraph on-chain
         B(Bridge)
         H(Hander)
      end
   end

   subgraph voting system
      C(Commission)
      V(VotingBooth)
      C ==> |2. originate| V
   end
```

### minor

+ removed those really unnecessary `modules`.
+ rewrite rule as a real function
+ it's named voting: [cBallotBox :=> VotingBooth]

## v0.2 - refactoried

+ keep using module for defining data structure
+ tested: whether things still work with only _partial\_sig_
   + it's a **no**: the non-polymorphic type of parameter in `CONTRACT 'p` limits the possibility.
+ callback can still implementable by using `contract sig` and `address`. This shouldn't be problem since all the `cBallotBox` should be created by a central voting commission.
+ re-design the procress of on-contract voting.
   + the Bridge in _v0.3_
+ added convention for naming contracts
   + contract files are prefixed by 'c'


## v0.1 - simplicity

+ using module from Liquidity for defining data structure
+ functions as first-class objs
+ the architecture of `tCommission` and `tCommittee`
+ the design of `tRule` and `tRulebase`
   + only do poc - timeless absolute majority rule
+ ~~operations as first-class objs~~
