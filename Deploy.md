

## current deployed contracts

+ commission:
   + `KT1Sefu81jFomBUTiJgK6VvCyY5rGrkhPszt`
+ bridge:
   + `KT1S9dDiWKfpHjsvtUq4iAzJm2dMLSwDeGyE`

## 3 steps for deployment

### deploy cCommission

compile and typecheck:

```
> liquidity cCommission.liq
> tezos-client -A node1.lax.tezos.org.sg typecheck script ./cCommission.tz
```

deploy cCommission:

```
tezos-client -A node1.lax.tezos.org.sg originate contract comm_0.3.1 for alias:dotblack transferring 50 from alias:dotblack running ./cCommission.tz --init '<data>' --burn-cap 20.0 -q -f > comm_0.3.1.out
```

### deploy cCommissionBridge

```
tezos-client -A node1.lax.tezos.org.sg originate contract commBridge_0.3.1 for alias:dotblack transferring 1 from alias:dotblack running ./cCommissionBridge.tz --init '(Pair False "KT1Sefu81jFomBUTiJgK6VvCyY5rGrkhPszt")' --burn-cap 20.0 -q -f > commBridge_0.3.1.out
```

### update cCommission

```
tezos-client -A node1.lax.tezos.org.sg transfer 1 from alias:dotblack to comm_0.3.1 --arg 'Right (Right (Right "KT1S9dDiWKfpHjsvtUq4iAzJm2dMLSwDeGyE"))' -q
```

## data definition

definition of `<data>`:

```
Pair { Elt "KT1BocfadR9e5EBJetqJUsqbFeMQJuatyhHN" Unit ;
       Elt "KT1DcEzmsZAmLyiSdtL8NMREDTKwkjnyuec8" Unit ;
       Elt "KT1RxMyx1jah6y9DU5XKp6zuYTGPgicGQeVn" Unit }
     (Pair "timeless_absMajority_80"
           (Pair {}
                 (Pair { Elt "timeless_absMajority_80"
                             { RENAME @sheet_slash_45 ;
                               DUP @sheet ;
                               SIZE @sizeVotes ;
                               DUP @sizeVotes ;
                               PUSH int 1 ;
                               MUL ;
                               NIL address ;
                               { DIP { DIP { DIP { DUP @sheet } ; SWAP } ; SWAP } ; SWAP } ;
                               ITER { RENAME @_fold_arg_slash_58 ;
                                      DIP { DUP } ;
                                      PAIR ;
                                      DUP ;
                                      DUP ;
                                      CDR @hs ;
                                      { DIP { DUP } ; SWAP } ;
                                      CAR ;
                                      CDR @v ;
                                      IF_LEFT
                                        { DROP ; DUP @hs }
                                        { IF_LEFT
                                            { DROP ; DUP @hs }
                                            { DROP ;
                                              DUP @hs ;
                                              { DIP { DIP { DUP } ; SWAP } ; SWAP } ;
                                              CAR ;
                                              CAR @k ;
                                              CONS } } ;
                                      DIP { DROP ; DROP ; DROP ; DROP } } ;
                               RENAME @vsN ;
                               SIZE @sizeVotedN ;
                               PUSH int 5 ;
                               MUL ;
                               COMPARE ;
                               GE ;
                               IF { PUSH (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative)))
                                         (Right (Right Unit)) }
                                  { DUP @sizeVotes ;
                                    PUSH int 4 ;
                                    MUL ;
                                    NIL address ;
                                    { DIP { DIP { DIP { DUP @sheet } ; SWAP } ; SWAP } ; SWAP } ;
                                    ITER { RENAME @_fold_arg_slash_56 ;
                                           DIP { DUP } ;
                                           PAIR ;
                                           DUP ;
                                           DUP ;
                                           CDR @hs ;
                                           { DIP { DUP } ; SWAP } ;
                                           CAR ;
                                           CDR @v ;
                                           IF_LEFT
                                             { DROP ; DUP @hs }
                                             { IF_LEFT
                                                 { DROP ;
                                                   DUP @hs ;
                                                   { DIP { DIP { DUP } ; SWAP } ; SWAP } ;
                                                   CAR ;
                                                   CAR @k ;
                                                   CONS }
                                                 { DROP ; DUP @hs } } ;
                                           DIP { DROP ; DROP ; DROP ; DROP } } ;
                                    RENAME @vsP ;
                                    SIZE @sizeVotedP ;
                                    PUSH int 5 ;
                                    MUL ;
                                    COMPARE ;
                                    GE ;
                                    IF { PUSH (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative)))
                                              (Right (Left Unit)) }
                                       { PUSH (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))) (Left Unit) } } ;
                               DIP { DROP ; DROP } } }
                       (Pair "KT1M4MPyaN1XFEKcbNWyZF7m1o1EjZHTEhsy" None))))
```
