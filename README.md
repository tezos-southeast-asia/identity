# Identity Solution

This is a repo for collecting all the _identity solution_ relating contracts, testing and documents.

Please read CHANGE.md or [devlog](https://Tezos-southeast-asia.gitlab.io/identity-devlog/) for more information.
